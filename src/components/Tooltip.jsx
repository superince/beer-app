const Tooltip = (props) => {
  const { message, children } = props;
  let tooltip = 'Ingredients: '
  for (const mess in message) {
    const i = Object.keys(message).indexOf(mess);
    i === 0 ? tooltip += `${mess}` : tooltip += `,${mess}`
  }
  return (
    <div className="group relative flex">
      <span className="absolute scale-0 transition-all rounded bg-gray-800 p-2 text-xs text-white group-hover:scale-100" style={{ top: "-50px" }}>{tooltip}</span>
      {children}
    </div>
  )
}

export default Tooltip;