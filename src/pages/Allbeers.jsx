import BeerCard from '../components/Card';

const AllBeers = (props) => {
  const { allBeers, loadMore } = props;
  return (
    <>
      {allBeers.length > 0 ? <div className="flex flex-col justify-center mt-5">
        <div className='grid sm:grid-cols-1 md:grid-cols-2 gap-4'>
          {allBeers.map((allBeer, i) => {
            return <div> <BeerCard beer={allBeer} key={i} /></div>
          })}
        </div>
        <button onClick={loadMore} className="my-5 text-blue-600 font-medium">Load More <i className="arrow down"></i></button>
      </div>
        :
        <div style={{height:"90vh"}}>
          <div className="flex justify-center items-center h-4/5">
            <img className="h-16 w-16" src="img/spinner.gif" alt="" />
          </div>
        </div>
      }
    </>
  )
}

export default AllBeers;