import { useForm } from "react-hook-form";

const Modal = (props) => {
  const { showModal, closeModal, addMyBeers } = props;
  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  const onSubmit = (data) => {
    addMyBeers(data);
    reset();
  }

  return (
    <>
      {showModal ? (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative my-6 mx-auto md:w-1/4 sm:w-1/4">
              <div className="border-0 rounded-md shadow-md relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <div className="flex items-start justify-between p-5 rounded-t">
                  <h3 className="text-2xl font-medium">
                    Add a New Beer
                  </h3>
                </div>
                <div className="relative px-6 flex flex-col">
                  <div className="flex justify-center h-28 w-28 border border-gray-200 bg-white rounded-md">
                    <img src="img/beer.png" alt="" className="h-full w-fit" />
                  </div>
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <input type="text" {...register("name", { required: true })} placeholder="Beer name*" className="mt-3 bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-md block w-full p-3 focus:outline-none capitalize" />
                    {errors.name && <span className="text-xs text-red-500">Beer name is required</span>}
                    <input type="text" {...register("tagline", { required: true })} placeholder="Genre*" className="mt-3 bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-md block w-full p-3 focus:outline-none capitalize" />
                    {errors.tagline && <span className="text-xs text-red-500">Genre is required</span>}
                    <textarea {...register("description", { required: true })} name="description" id="" cols="30" rows="10" placeholder="Description*" className="mt-3 bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-md block w-full p-3 focus:outline-none"></textarea>
                    {errors.description && <span className="text-xs text-red-500">Description is required</span>}
                    <div className="flex items-center justify-end p-6 rounded-b">
                      <button
                        className="text-gray-400 background-transparent font-bold px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                        type="button"
                        onClick={() => { reset(); closeModal(false) }}
                      >
                        Cancel
                      </button>
                      <button
                        className="bg-blue-600 text-white active:bg-emerald-600 font-bold text-sm px-8 py-2 rounded hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                        type="submit"
                      >
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}

export default Modal;