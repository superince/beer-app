
import { Routes, Route, NavLink } from 'react-router-dom';
import AllBeers from "./pages/Allbeers";
import MyBeers from "./pages/Mybeers";
import Modal from "./components/Modal";
import { useState, useEffect } from "react";
import axios from 'axios';
import './App.css';

let perPage = 6;
function App() {
  const [showModal, setShowModal] = useState(false);
  const [addButton, setaddButton] = useState(false);
  const [allBeers, setAllBeers] = useState([]);
  const [myBeers, setMyBeers] = useState([]);

  useEffect(() => {
    fetchAllBeers(perPage)
    fetchMyBeers()
  }, []);

  const fetchMyBeers = () => {
    const rawBeers = localStorage.getItem("mybeers");
    if (rawBeers) {
      const jsonBeers = JSON.parse(rawBeers)
      setMyBeers(jsonBeers)
    }
  }

  const fetchAllBeers = (perPage) => {
    axios.get(`https://api.punkapi.com/v2/beers?page=1&per_page=${perPage}`)
      .then(({ data }) => {
        setAllBeers(data);
      })
  }

  const loadMore = async () => {
    perPage += 10;
    fetchAllBeers(perPage)
  }

  const toggleModal = () => {
    setShowModal(prevShowModal => !prevShowModal);
  }

  const toggleAddButton = () => {
    setaddButton(prevAddButton => !prevAddButton);
  }

  const addMyBeers = (data) => {
    const myBeers = localStorage.getItem("mybeers");
    data['image_url'] = "/img/beer.png";
    if (myBeers) {
      const jsonData = JSON.parse(myBeers);
      jsonData.push(data);
      setMyBeers(jsonData);
      localStorage.setItem("mybeers", JSON.stringify(jsonData));
    } else {
      const newBeer = [];
      newBeer.push(data);
      setMyBeers(newBeer);
      localStorage.setItem("mybeers", JSON.stringify(newBeer));
    }
    toggleModal();
  }

  return (
    <div className='container mx-auto p-6'>
      <div className='flex items-center justify-between'>
        <div className='flex space-x-6 mt-3'>
          <NavLink to="/" className='hover:text-darkGrayishBlue'>All Beers</NavLink>
          <NavLink to="/my-beers" className='hover:text-darkGrayishBlue'>My Beers</NavLink>
        </div>
        {addButton ? <button className='bg-blue-700 px-3 py-2 text-white font-medium rounded-sm shadow hover:shadow-lg' onClick={toggleModal}>Add a new beer</button> : ''}
      </div>

      <Routes>
        <Route path="/" element={<AllBeers allBeers={allBeers} loadMore={loadMore} />} />
        <Route path="/my-beers" element={<MyBeers openModal={toggleModal} toggleAddButton={toggleAddButton} myBeers={myBeers} />} />
      </Routes>

      <Modal showModal={showModal} closeModal={toggleModal} addMyBeers={addMyBeers} />
    </div>
  );
}

export default App;
