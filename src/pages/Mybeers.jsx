import BeerCard from '../components/Card';
import { useEffect } from 'react';

const MyBeers = (props) => {
  const { openModal, myBeers, toggleAddButton } = props;

  useEffect(() => {
    toggleAddButton()
    return () => {
      toggleAddButton()
    }
  }, [toggleAddButton]);

  return (
    <div className='flex flex-col justify-center mt-5'>
      {myBeers.length > 0 ? myBeers.map((myBeer, i) => {
        return <BeerCard beer={myBeer} key={i} />
      }) :
        <div className='flex flex-col items-center justify-center w-full bg-gray-100 rounded-md' style={{ height: "85vh" }}>
          <p>No thing to see yet.</p>
          <p><button className='text-blue-600' onClick={openModal}>Click here</button> to add your first beer!</p>
        </div>
      }
    </div>
  )
}

export default MyBeers;