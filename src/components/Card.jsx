import Tooltip from "./Tooltip";

const BeerCard = (props) => {
  const { beer } = props;
  return (
    <div className="flex flex-row justify-start shadow-3xl shadow-gray-300 p-6 my-2 rounded-sm hover:shadow-none hover:bg-sky-100 hover:cursor-pointer">
      {beer.ingredients ?
        <Tooltip message={beer.ingredients}>
          <div className="flex justify-center h-32 w-32 pr-5">
            <img src={beer.image_url} alt="" className="h-full w-fit" />
          </div>
        </Tooltip> :
        <div className="flex justify-center h-32 w-32 pr-5">
          <img src={beer.image_url} alt="" className="h-full w-fit" />
        </div>
      }
      <div className="flex flex-col basis-full">
        <h1 className="font-medium text-2xl mb-2">{beer.name}</h1>
        <h6 className="text-accentColor font-medium text-sm mb-2">{beer.tagline}</h6>
        <p className="text-sm dotdot">{beer.description}</p>
      </div>
    </div>
  )
}

export default BeerCard;